# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FixCorruptLayersDialog
                                 A QGIS plugin
 this plugin will fix your corrupted layers
                             -------------------
        begin                : 2015-06-23
        git sha              : $Format:%H$
        copyright            : (C) 2015 by mattia dorigo
        email                : mattiadorigo@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import time
import os, fnmatch
import xml.etree.ElementTree as ET
from PyQt4.QtCore import pyqtSlot
from PyQt4 import QtGui, uic
from PyQt4.QtGui import QFileDialog
import subprocess
from string import join

from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

from pyspatialite import dbapi2 as sqlite3

#useful imports
import zipfile
import shutil
from _ast import arguments
import re

#enviroment path
plugin_dir = os.path.dirname(__file__)
temp_dir = os.path.join(plugin_dir, 'tmp')
data_dir = os.path.join(plugin_dir, 'data')

variousInfo = []
variousInfo.append(1)
variousInfo.append(0)
variousInfo.append(0)

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'fix_corrupt_layers_dialog_base.ui'))

def find_files(directory, pattern):
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                yield filename

def FindPath(HomeDir,nameFile):
    print HomeDir
    print nameFile
    path = ""
    for filename in find_files(HomeDir,nameFile):
        path = filename
    return path
    
def ChangePathDB(stringa,percorso):
    single = stringa
    #print single
    p = re.search(r'dbname=[\'"](.[^\s]*?)[\'"]',single)

    #se il file esiste al percorso indicato, lo lascio stare
    if p:
        print "ciaociao"
        
        if os.path.exists(p.group(1)):
            print "percorso esistente"
            pass
        else:
            textname = os.path.basename(p.group(1))
            path = FindPath(percorso,textname)
            path =  path.replace("\\", "/")
            string2 = single.replace(p.group(1),path)
            single = string2
            variousInfo[1] = variousInfo[1] + 1

    else:
        pass
    #print "stringa : " + single
    return single

def ChangePathSHP(stringa,percorso):

    single = stringa
    single = single.split("|")
    #se il file esiste al percorso indicato, lo lascio stare
    if os.path.exists(single[0]):
        final = "|".join(single)
        pass
        #se non esiste, ci lavoro sopra
    else:
        #da un path prendo solo il nome del file
        textname = os.path.basename(single[0])
        #con le funzioni sopra, cerco il file da una directory che ho dato
        path = FindPath(percorso,textname)
        #sostituisco i backslash con lo slash
        path =  path.replace("\\", "/")
        single[0] = path
        #ricostruisco il percorso, aggiungendo eventualmente altri file
        final =  "|".join(single)
        variousInfo[2] = variousInfo[2] + 1

    return final

class FixCorruptLayersDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""

        super(FixCorruptLayersDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        self.file_src_path = "C:/Documents and Settings/dor/Desktop/prova/prova.qgs"
        self.file_dst_path = "C:/Documents and Settings/dor/Desktop/prova/"
        variousInfo[0] = self.lineEdit3
    
    @pyqtSlot()
    def on_SelectFileQGS_clicked(self):

        self.file_src_path = QFileDialog.getOpenFileName(None,"seleziona file gqs")
        self.lineEdit3.setText("file qgs = " +self.file_src_path + "\n")
        self.lineEdit3.setText(self.lineEdit3.toPlainText() + "directory layer = " +self.file_dst_path + "\n")
        
    @pyqtSlot()
    def on_SelectDirLayer_clicked(self):

        self.file_dst_path = QFileDialog.getExistingDirectory(None,"seleziona directory layer")
        self.lineEdit3.setText("file qgs = " +self.file_src_path + "\n")
        self.lineEdit3.setText(self.lineEdit3.toPlainText() + "directory layer = " +self.file_dst_path + "\n")
    
    @pyqtSlot()
    def on_CorrectLayouts_clicked(self):
        
        self.lineEdit3.setText(self.file_src_path +"\n" + self.file_dst_path + "\nfatto")
        tree = ET.parse(self.file_src_path)
        root = tree.getroot()
        print "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\----------/////////////////"    
        # ciclo che mi permette di ottenere tutti i datasource del file xml
        for ds in root.iter('datasource'):
            single = ds.text

            if "dbname=" in single:
                single = ChangePathDB(single,self.file_dst_path)
                ds.text = single
                print single

            if ".shp" in single:
                single = ChangePathSHP(single,self.file_dst_path)
                ds.text = single

        self.lineEdit3.setText("layer shape modificati = " +str(variousInfo[2]) + "\n")
        self.lineEdit3.setText(self.lineEdit3.toPlainText() + "layer DB modificati = " + str(variousInfo[1]) + "\n")
        self.lineEdit3.setText(self.lineEdit3.toPlainText() + "finito")
        variousInfo[1] = 0
        variousInfo[2] = 0
        tree.write(os.path.dirname(self.file_src_path) + "/modified.qgs")