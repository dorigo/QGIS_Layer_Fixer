# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FixCorruptLayers
                                 A QGIS plugin
 this plugin will fix your corrupted layers
                             -------------------
        begin                : 2015-06-23
        copyright            : (C) 2015 by mattia dorigo
        email                : mattiadorigo@gmail.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load FixCorruptLayers class from file FixCorruptLayers.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .fix_corrupt_layers import FixCorruptLayers
    return FixCorruptLayers(iface)
