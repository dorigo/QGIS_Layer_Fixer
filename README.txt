this plugin allows you to save time with your qgis software.
When you move your .shp layers or .sqlite from a folder to another one, you have to correct manually the path.
When you have lots of layers files moved, with this plugin you can select a new root directory for all your "not found" layers.
You just need to select your qgis file, your layer root directory and run the script. It will recursively search the given directory for each datasource file.
IT works only with spatialite (.sqlite) and shapefile datasources (.shp).

Questo plugin ti permette di non perdere tempo con il software qgis.
Qyando sposti i tuoi file .shp o .sqlite da una cartella a un'altra, devi correggere manualmente il percorso.
Quando hai tanti layer spostati, con questo plugin puoi selezionare una nuova root directory per tutti i tuoi file "non trovati".
Devi solo selezionare il file qgis, la cartella che contiene tutti i tuoi layer e far partire lo script. Il plugin cercherà ricorsivamente i file dentro la root directory.
Funziona solo con file spatialite (.sqlite) e shapefile (.shp).